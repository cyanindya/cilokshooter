﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Difficulty_New", menuName="Scriptable Objects/Difficulty Settings")]
public class DifficultySettings : ScriptableObject
{
    public new string name = "New Difficulty";
    public float accelerationMultiplier = 1.0f;
    public float spawnInterval = 5.0f; // this one goes to Spawner object
    public int maxHealth = 5;
}
