﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Targetable/Effect/Add Score")]
public class AddScoreEffect : TargetableEffect
{
    public override void ApplyEffect()
    {
        Session session = FindObjectOfType<Session>();
        if (session != null)
        {
            session.totalScore += effectRate;
        }
    }
}
