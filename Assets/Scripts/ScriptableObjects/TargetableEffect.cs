﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TargetableEffect : ScriptableObject
{
    public int effectRate = 0;

    public abstract void ApplyEffect();
}
