﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Targetable/Effect/HP Heal")]
public class RestoreHealthEffect : TargetableEffect
{
    public override void ApplyEffect()
    {
        Session session = FindObjectOfType<Session>();
        if (session != null)
        {
            session.currentHealth += effectRate;
        }
    }
}
