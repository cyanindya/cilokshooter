﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Manager_New", menuName = "Scriptable Objects/Game Manager")]
public class GameManager : ScriptableObject
{
    // public DifficultySettings currentDifficulty;
    public List<DifficultySettings> difficulties = new List<DifficultySettings>();

    private void Awake()
    {
        //if (currentDifficulty == null && difficulties != null)
        //{
        //    currentDifficulty = difficulties[0];
        //}
    }

    //public void SetDifficulty(int difficultyIndex)
    //{
    //    currentDifficulty = difficulties[difficultyIndex];
    //    Debug.Log("Set to " + currentDifficulty.name + " difficulty.");
    //}
}
