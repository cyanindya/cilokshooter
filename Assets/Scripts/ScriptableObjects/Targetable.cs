﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Targetable/Targetable")]
public class Targetable : ScriptableObject
{
    public float firingAngle = 45f;
    public float acceleration = Physics.gravity.magnitude; // set x1.0 as multiplier
    public TargetableEffect targetableEffect;
    
}
