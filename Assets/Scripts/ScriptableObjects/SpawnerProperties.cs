﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Spawner Properties")]
public class SpawnerProperties : ScriptableObject
{
    public GameObject[] spawnables; // note that the spawning is in uniform distribution
    public float defaultDelay = 5f;
}
