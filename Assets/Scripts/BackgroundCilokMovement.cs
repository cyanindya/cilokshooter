﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundCilokMovement : MonoBehaviour
{

    RectTransform rect;

    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        rect.anchoredPosition = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = rect.anchoredPosition;
        pos.x -= Time.deltaTime * 50f;
        pos.y -= Time.deltaTime * 50f;
        rect.anchoredPosition = pos;

        //if (rect.anchoredPosition.x < -0.04 * Screen.width)
        //{
        //    Destroy(gameObject);
        //}
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
