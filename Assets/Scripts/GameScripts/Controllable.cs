﻿using UnityEngine;

public class Controllable : MonoBehaviour
{

    public float raycastRange = 100f;
    public float raycastRadius = 0.25f;
    RaycastHit targetedObject;
    public Target target;
    //public ParticleSystem flashIndicator;

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, transform.up, Color.blue);
        //Ray ray = new Ray(transform.position, transform.up);

        //if (Physics.Raycast(ray, out targetedObject, raycastRange))
        //{
        //    target = targetedObject.transform.GetComponent<Target>();
        //    if (target != null)
        //    {
        //        // Check raycast angle
        //        // Debug.Log("A cilok is detected.");
        //        Debug.Log(Vector3.Angle(ray.direction, targetedObject.transform.forward));

        //        if (Input.GetButtonDown("Fire1"))
        //        {
        //            GrabTarget(target);
        //        }
        //    }
        //}

        if (Physics.SphereCast(transform.position, raycastRadius, transform.up, out targetedObject, raycastRange))
        {
            target = targetedObject.transform.GetComponent<Target>();
            if (target != null)
            {
                // Check raycast angle
                // Debug.Log("A cilok is detected.");

                // TODO: don't need trigger, so only need to point?

                if (Input.GetButtonDown("Fire1"))
                {
                    GrabTarget(target);
                }
            }
        }
    }

    void GrabTarget(Target tgt)
    {
        //flashIndicator.Play();
        tgt.isShot = true;
        Debug.Log("Target shot.");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Target>() != null)
        {

        }
    }
}
