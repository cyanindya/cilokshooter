﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    Session currentSession;

    //public float speedScaler = 1.0f;
    Rigidbody rb;

    public Targetable targetParameterData;

    //public Transform firingTarget;
    //public float firingAngle = 45.0f;
    //public float acceleration = Physics.gravity.magnitude;
    float firingAngle;
    float acceleration;
    public bool isShot = false;

    Vector3 startingPoint = Vector3.zero;
    Vector3 endPoint = Vector3.zero;
    Vector3 movementVelocity = Vector3.zero;
    Vector3 tempTargetPos = Vector3.zero;

    float elapsedTime = 0.0f;


    private void Awake()
    {
        // Get session data so we can access the difficulty setting
        currentSession = FindObjectOfType<Session>();
        int difficultyLevel = PlayerPrefs.GetInt("DifficultyIndex", 0);
        DifficultySettings difficulty = currentSession.manager.difficulties[difficultyLevel];

        if (difficulty != null)
        {
            // Load data from scriptable objects
            if (targetParameterData != null)
            {
                firingAngle = targetParameterData.firingAngle;
                acceleration = targetParameterData.acceleration * difficulty.accelerationMultiplier;
            }
            else
            {
                firingAngle = 45.0f;
                acceleration = Physics.gravity.magnitude * difficulty.accelerationMultiplier;
            }
        }

        // Set initial position
        rb = gameObject.GetComponent<Rigidbody>();
        if (rb != null)
        {
            //rb.useGravity = false;
            startingPoint = rb.transform.position;
        }
        else
        {
            startingPoint = transform.position;
        }
        tempTargetPos = startingPoint;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb != null)
        {
            //if (isShot) moveToController();
            if (isShot) hitReaction();
            else moveToTarget();
            //moveToController();
        }

        //Debug.DrawLine(startingPoint, endPoint, Color.black);
        //Debug.DrawLine(startingPoint, tempTargetPos, Color.green);
        //Debug.DrawLine(transform.position, transform.forward, Color.red);
    }

    public void shotTarget()
    {
        isShot = true;
        Debug.Log("Target shot.");
    }

    void hitReaction()
    {
        if (targetParameterData != null)
        {
            targetParameterData.targetableEffect.ApplyEffect();
        }

        // TODO: add to the session's hit list?

        // TODO: hit particle
        // Disable the game object - do this instead of destroy?
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    //void moveToController()
    //{
    //    Debug.Log("Moving to controller");
    //    Controllable controller = Camera.main.GetComponentInChildren<Controllable>();
    //    if (controller != null)
    //    {
    //        // Make sure the cannonball faces the destination
    //        if (rb != null)
    //        {
    //            rb.transform.LookAt(controller.transform.position);
    //        }
    //        else
    //        {
    //            transform.LookAt(controller.transform.position);
    //        }

    //        if (rb != null)
    //        {
    //            rb.useGravity = false;
    //            rb.transform.position = Vector3.MoveTowards(rb.transform.position, Camera.main.transform.position, Time.deltaTime * 10f);
    //        }
    //        else
    //        {
    //            transform.position = Vector3.MoveTowards(transform.position, controller.transform.position, Time.deltaTime * 10f);
    //        }
    //    }
    //}

    // By default, move the cilok balls like a cannonball in parabolic trajectory.
    // See https://www.desmos.com/calculator/gm3scd9uij, https://en.wikipedia.org/wiki/Range_of_a_projectile,
    // https://unity3d.college/2017/06/30/unity3d-cannon-projectile-ballistics/, and many others.
    void moveToTarget()
    {
        tempTargetPos = startingPoint + movementVelocity * elapsedTime;
        tempTargetPos.y -= 0.5f * Mathf.Abs(acceleration) * elapsedTime * elapsedTime;

        if (rb != null)
        {
            //rb.transform.position += movementVelocity * elapsedTime;
            rb.transform.position = tempTargetPos;
            //rb.AddForce(movementVelocity * Time.deltaTime, ForceMode.Impulse);
        }
        else
        {
            //transform.position += movementVelocity * Time.deltaTime;
            transform.position = tempTargetPos;
        }

        elapsedTime += Time.fixedDeltaTime; // don't forget to update the timestep to 0.001 or so to remove jagged movement (dirty fix)
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision detected.");
        float collisionReactTime = 0.0f;
        while (collisionReactTime < 2.0f) collisionReactTime += Time.fixedDeltaTime; // this instead of deltaTime?

        elapsedTime = 0.0f;

        // Triggered when the object hits something (thus player fails to hit)
        Debug.Log("Target missed!");
        currentSession.currentHealth -= 1;
        Destroy(gameObject);
    }

    public void spawnObject(Transform destination)
    {
        endPoint = destination.position;

        // Make sure the cannonball faces the destination
        if (rb != null)
        {
            rb.transform.LookAt(endPoint);
        }
        else
        {
            transform.LookAt(endPoint);
        }

        // Get the direction between the starting point and end point
        Vector3 shotDirection = endPoint - startingPoint;

        // Get the height difference
        float height = shotDirection.y;

        // Reset the height as we will manually edit this as we go.
        shotDirection.y = 0;

        // Calculate horizontal distance of the shot
        float shotDistance = shotDirection.magnitude;

        // Convert shot angle
        float shotAngleRadian = firingAngle * Mathf.Deg2Rad;

        // Starting vertical firing position
        //shotDirection.y = shotDistance * Mathf.Tan(shotAngleRadian);
        //shotDistance += height * Mathf.Tan(shotAngleRadian); // correction
        shotDirection.y = shotDistance * Mathf.Sin(shotAngleRadian);
        shotDistance += height * Mathf.Sin(shotAngleRadian); // correction

        // Set the final velocity
        float velocity = Mathf.Sqrt(shotDistance * Mathf.Abs(acceleration) / Mathf.Sin(2f * shotAngleRadian));
        movementVelocity = velocity * shotDirection.normalized;


    }

}
