﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Session : MonoBehaviour
{
    public GameManager manager;
    public int currentHealth;
    private int maxHealth;
    public GameObject[] caughtItems;
    public int totalScore;
    public bool ongoing = true;

    // game over management
    public Canvas playUI;
    public GameObject gameOverObject;
    public GameObject retryObject;

    private void Awake()
    {
        if (manager != null)
        {
            //maxHealth = manager.currentDifficulty.maxHealth;
            maxHealth = manager.difficulties[PlayerPrefs.GetInt("DifficultyIndex", 0)].maxHealth;
            currentHealth = maxHealth;
            totalScore = 0;
        }
    }

    private void Update()
    {
        // If health reaches 0, trigger GameOver
        if (currentHealth <= 0 && ongoing)
        {
            ongoing = false;
            GameOver();
        }
    }

    // Delegate score update and health modification to other components

    private void GameOver()
    {
        // SceneManager.LoadScene("GameOverScene");
        playUI.gameObject.SetActive(false);
        gameOverObject.SetActive(true);
    }

    public void RetryPrompt()
    {
        gameOverObject.SetActive(false);
        retryObject.SetActive(true);
    }
}
