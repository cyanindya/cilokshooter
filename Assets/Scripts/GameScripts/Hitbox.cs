﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    private Vector3 RandomVector(float min, float max)
    {
        var x = Random.Range(min, max);
        var y = 0.0f;
        var z = Random.Range(min/2.0f, max/2.0f);
        return new Vector3(x, y, z);
    }
    private Transform hitboxPosition;
    private float timer;
    private Vector3 destination;

    public float delay = 4.0f;
    public float speed = 2.0f;
    
    public bool waitForDirectHit = false;

    public float maxLRBoundary = 1.0f;
    public float maxFBBoundary = 1.0f;

    private void Awake()
    {
        hitboxPosition = transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        destination = hitboxPosition.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!waitForDirectHit && timer > delay)
        {
            setNextDestination();
            //Debug.Log("Next position: " + destination.ToString());
            timer -= delay;
        }

        hitboxPosition.position = Vector3.MoveTowards(hitboxPosition.position, destination, Time.deltaTime * speed);

        if (!waitForDirectHit)
        {
            timer += Time.deltaTime;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        setNextDestination();

        if (!waitForDirectHit) timer = 0.0f;
    }

    private void setNextDestination()
    {
        destination = hitboxPosition.position + RandomVector(-maxLRBoundary, maxLRBoundary);
        if (destination.x > maxLRBoundary)
        {
            destination.x = maxLRBoundary;
        }
        else if (destination.x < -maxLRBoundary)
        {
            destination.x = -maxLRBoundary;
        }
        if (destination.z > maxFBBoundary)
        {
            destination.z = maxFBBoundary;
        }
        else if (destination.z < -maxFBBoundary)
        {
            destination.z = -maxFBBoundary;
        }
    }

}
