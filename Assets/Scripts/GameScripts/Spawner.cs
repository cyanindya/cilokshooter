﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // This version is temporarily copied from the Beat Saber VR 10-minute challenge by Valem
    // Explanations:
    // - spawnables - contains a list of cilok types to be spawned randomly. Let's say we have a regular cilok and a hot cilok.
    // - spawnPoints - contains a list of where the spawn points should be located.
    // - delay - this is slightly misleading, but basically the amount of time the timer must be reached before the next cilok is spawned.
    // - timer - the time tracker that gradually builds up in number until the next cilok is spawned once its number is larger than countdown.

    public Session currentSession;

    public SpawnerProperties spawnerProperties;

    public GameObject[] spawnables;
    public Transform[] spawnPoints;
    public float delay = (60 / 130) * 2;
    private float timer;

    private int numberOfSpawnables;
    private int numberOfSpawnPoints;

    public Transform firingTarget;

    Animator spawnerAnimation;
    //bool animationIsPlaying = false;

    private void Awake()
    {
        // Get session data so we can access the difficulty setting
        currentSession = FindObjectOfType<Session>();
        int difficultyLevel = PlayerPrefs.GetInt("DifficultyIndex", 0);
        DifficultySettings difficulty = currentSession.manager.difficulties[difficultyLevel];

        if (difficulty != null)
        {
            delay = difficulty.spawnInterval;
        }
        else
        {
            if (spawnerProperties != null)
            {
                delay = spawnerProperties.defaultDelay;
            }
        }

        // Automatically get spawner data if available
        if (spawnerProperties != null)
        {
            spawnables = spawnerProperties.spawnables;
        }

        // Get a spawner model if available
        spawnerAnimation = GetComponentInChildren<Animator>();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        // Make sure timer is reset
        timer = 0.0f;

        // Get the number of items in the list of spawnables and spawn points
        numberOfSpawnables = spawnables.Length;
        numberOfSpawnPoints = spawnPoints.Length;

    }

    // Update is called once per frame
    void Update()
    {
        // Make sure the spawner only behaves when the session is active
        if (currentSession != null && currentSession.ongoing)
        {

            // Once the timer builds up enough...

            if (timer > delay + 2f)
            {
                // FIXME: very crude fix and NOT recommended to use coroutine.
                // Is there a way to properly wait for the animation to finish playing,
                // execute the program, THEN reset the controller?
                //// spawnNewTarget();
                StartCoroutine(playSpawnerAnimation());

                // Reset the timer by decreasing it by the countdown limit
                timer -= delay + 2f;
            }
            timer += Time.deltaTime;

        }
        
    }

    void spawnNewTarget()
    {

        // Choose randomly on which cilok type to spawn and where to spawn (though right now, we only have one type)
        GameObject cilok = Instantiate(spawnables[Random.Range(0, numberOfSpawnables)], spawnPoints[Random.Range(0, numberOfSpawnPoints)]);

        // Set the local tracked position of the new cilok at zero
        cilok.transform.localPosition = Vector3.zero;
        cilok.transform.Rotate(0f, 180f, 0f);

        // Delegate movement to each babies.
        if (cilok.GetComponent<Target>() != null)
        {
            Target tgt = cilok.GetComponent<Target>();
            tgt.spawnObject(firingTarget);
        }

    }

    IEnumerator playSpawnerAnimation()
    {
        //animationIsPlaying = true;

        if (spawnerAnimation != null)
        {
            int spawningAnimationHash = Animator.StringToHash("Spawning");
            int idleAnimationHash = Animator.StringToHash("Idle");
            Debug.Log("Spawning animation hash: " + spawningAnimationHash.ToString());
            Debug.Log("Idle animation hash: " + idleAnimationHash.ToString());

            spawnerAnimation.SetTrigger("spawning");

            Debug.Log("Current hash:" + spawnerAnimation.GetCurrentAnimatorStateInfo(0).shortNameHash.ToString());

            while (spawnerAnimation.GetCurrentAnimatorStateInfo(0).shortNameHash != spawningAnimationHash)
            {
                Debug.Log("Transitioning...");
                yield return null;
            }
            Debug.Log("Transition complete.");

            float localCounter = 0;
            float waitTime = spawnerAnimation.GetCurrentAnimatorStateInfo(0).length;

            Debug.Log("State: " + spawnerAnimation.GetCurrentAnimatorStateInfo(0).ToString() + ", delay: " + waitTime.ToString());

            while (localCounter < waitTime - 0.5f)
            {
                localCounter += Time.deltaTime;
                yield return null;
            }

            spawnerAnimation.ResetTrigger("spawning");

        }

        //animationIsPlaying = false;

        spawnNewTarget();
        
    }

}
