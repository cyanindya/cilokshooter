﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairBehavior : MonoBehaviour
{
    public Controllable targeter;
    public GameObject idleCrosshair;
    public GameObject activeCrosshair;
    Target targetInRange;

    private void Start()
    {
        idleCrosshair.SetActive(true);
        activeCrosshair.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        targetInRange = targeter.target;

        if (targetInRange != null)
        {
            idleCrosshair.SetActive(false);
            activeCrosshair.SetActive(true);
        }
        else
        {
            idleCrosshair.SetActive(true);
            activeCrosshair.SetActive(false);
        }
    }
}
