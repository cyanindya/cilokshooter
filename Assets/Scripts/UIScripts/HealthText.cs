﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthText : MonoBehaviour
{
    Session currentSession;
    Text scoreText;

    private void Awake()
    {
        currentSession = FindObjectOfType<Session>();
        scoreText = transform.GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (scoreText != null && currentSession != null)
        {
            scoreText.text = currentSession.currentHealth.ToString();
        }
    }
}
