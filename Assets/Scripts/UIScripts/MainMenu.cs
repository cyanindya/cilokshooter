﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameManager gameManager;

    private void Awake()
    {
        // Check if PlayerPrefs has Difficulty setting stored. If not, start out with normal difficulty (code: 0)
        if (!PlayerPrefs.HasKey("DifficultyIndex"))
        {
            PlayerPrefs.SetInt("DifficultyIndex", 0);
            PlayerPrefs.Save();
        }
    }

    public void PlayGame()
    {
        // Debug.Log("Playing in " + gameManager.currentDifficulty.name + " difficulty");
        Debug.Log("Playing in " + gameManager.difficulties[PlayerPrefs.GetInt("DifficultyIndex", 0)].name + " difficulty");
        SceneManager.LoadScene("PlayScene");
    }
    
    public void Settings(SettingsMenu settings)
    {
        // SceneManager.LoadScene("DifficultySelectionScene");
        
        if (settings != null)
        {
            Debug.Log("Settings available");
            gameObject.SetActive(false);
            settings.gameObject.SetActive(true);
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false; // allow quitting in editor
#endif
        Application.Quit();
    }
}
