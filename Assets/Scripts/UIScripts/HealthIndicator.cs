﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthIndicator : MonoBehaviour
{
    Image[] livesBar;
    Session currentSession;
    public Image singleHPIndicator;
    int maxLives;
    // float timer = 0f;

    private void Awake()
    {
        currentSession = FindObjectOfType<Session>();
        maxLives = currentSession.manager.difficulties[PlayerPrefs.GetInt("DifficultyIndex", 0)].maxHealth;
    }

    // Start is called before the first frame update
    void Start()
    {
        livesBar = new Image[maxLives];
        for (int i = 0; i < maxLives; i++)
        {
            livesBar[i] = Instantiate(singleHPIndicator, Vector3.zero, Quaternion.identity);
            // livesBar[i].transform.SetParent(gameObject.transform);
            livesBar[i].transform.SetParent(gameObject.transform, false);
            livesBar[i].rectTransform.anchoredPosition = new Vector2(i * livesBar[i].rectTransform.localScale.x * livesBar[i].rectTransform.rect.width * 0.75f, 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // TEST
        //if (timer >= 10f)
        //{
        //    currentSession.currentHealth -= 1;
        //    timer = 0f;
        //}
        for (int i = 0; i < maxLives; i++)
        {
            if (i < currentSession.currentHealth)
            {
                livesBar[i].enabled = true;
            }
            else
            {
                livesBar[i].enabled = false;
            }
        }
        //timer += Time.deltaTime;
    }
}
