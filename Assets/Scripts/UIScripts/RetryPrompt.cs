﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryPrompt : MonoBehaviour
{
    // Start is called before the first frame update
    public void RetryConfirmPrompt()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void RetryCancelPrompt()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
