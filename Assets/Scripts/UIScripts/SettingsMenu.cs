﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour
{
    public GameManager gameManager;

    public void SelectDifficulty(int difficultyIndex)
    {
        // gameManager.SetDifficulty(difficultyIndex);
        PlayerPrefs.SetInt("DifficultyIndex", difficultyIndex);
        PlayerPrefs.Save();

        Debug.Log("Set to " + gameManager.difficulties[difficultyIndex].name + " difficulty.");
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButtonDown("Cancel"))
        //{
        //    //SceneManager.LoadScene("MainMenuScene");
        //    MainMenu main = FindObjectOfType<MainMenu>();
        //    if (main != null)
        //    {
        //        gameObject.SetActive(false);
        //        main.gameObject.SetActive(true);
        //    }
        //}
    }

    public void ReturnToMainMenu(MainMenu main)
    {
        if (main != null)
        {
            gameObject.SetActive(false);
            main.gameObject.SetActive(true);
        }
    }
}
