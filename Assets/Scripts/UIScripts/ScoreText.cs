﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreText : MonoBehaviour
{

    Session currentSession;
    Text scoreText;
    TextMeshProUGUI scoreTextMesh;

    private void Awake()
    {
        currentSession = FindObjectOfType<Session>();
        scoreText = transform.GetComponent<Text>();
        scoreTextMesh = transform.GetComponent<TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (scoreTextMesh != null && currentSession != null)
        {
            Debug.Log(scoreTextMesh.text);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scoreText != null && currentSession != null)
        {
            scoreText.text = currentSession.totalScore.ToString();
        }
        if (scoreTextMesh != null && currentSession != null)
        {
            scoreTextMesh.SetText(currentSession.totalScore.ToString());
        }
    }
}
